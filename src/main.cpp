#include <iostream>
#include <thread>
#include "MorphInterface.hpp"
#include <unistd.h>
int main(){

  sensel::MorphInterface oneMorph(0, false);
  oneMorph.startScanning();

  //ToDo: document that if you want to modify your morph instance you should capture a ref
  std::thread senselThread(&sensel::MorphInterface::handleRead, &oneMorph,
    [&](sensel::Frame xy){
      if(xy.n_contacts > 0){
				  // std::cout << "Num Contacts:" << m_pframe->n_contacts << '\n';

				for (int c = 0; c < xy.n_contacts; c++){
					unsigned int state = xy.contacts[c].state;
			
					if(state == CONTACT_START){
						oneMorph.setLED(xy.contacts[c].id,100);
					}else if(state == CONTACT_END){
						oneMorph.setLED(xy.contacts[c].id,0);
					}
          std::cout << "X:" <<  xy.contacts[c].x_pos << '\n';
				}
      }
    });
  //std::thread senselThread(&sensel::MorphInterface::read, &oneMorph);

  senselThread.join();
  return 0;
}
