#include "MorphInterface.hpp"

namespace sensel {

MorphInterface::MorphInterface(int deviceID, bool startScanningImmediately){

  //Get a list of avaialble Sensel devices
  senselGetDeviceList(&m_list);
  if(m_list.num_devices == 0){
    std::cout <<  "No/'/..;. device found\n";
  }else{
    std::cout << "Found a Sensel\n";
  }
    //Open a Sensel device by the id in the SenselDeviceList, handle initialized 
  senselOpenDeviceByID(&m_handle, m_list.devices[deviceID].idx);
  //Get the sensor info
  senselGetSensorInfo(m_handle, &m_sensorInfo);

  //We can default a common initialisation here and add the ability to change this later
  senselSetFrameContent(m_handle, FRAME_CONTENT_CONTACTS_MASK);
  senselAllocateFrameData(m_handle, &m_pframe); 
  senselSetDynamicBaselineEnabled(m_handle, false);

  if(startScanningImmediately){
    senselStartScanning(m_handle);
  }
}

void MorphInterface::startScanning(){
  senselStartScanning(m_handle);
}

void MorphInterface::setLED(unsigned char ledID, unsigned short brightness){
  senselSetLEDBrightness(m_handle, ledID, brightness);
}

void MorphInterface::handleRead(std::function<void (Frame)> readCallback){
    while(m_exit){
      unsigned int num_frames = 0;
      //Read all available data from the Sensel device
      senselReadSensor(m_handle);
      //Get number of frames available in the data read from the sensor
      senselGetNumAvailableFrames(m_handle, &num_frames);
      for (int f = 0; f < num_frames; f++){
        //Read one frame of data
        senselGetFrame(m_handle, m_pframe);
        readCallback(*m_pframe);
      }
    }
}

} //sensel