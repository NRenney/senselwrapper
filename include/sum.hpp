/**
 * Author: Benedict R. Gaster
 * Desc: Create vistor for variants using lambdas 
 *
 * Note: The code below mostly comes from the blog:
 *  https://bitbashing.io/std-visit.html
 *
 * LICENSE: See the above link and blog.
 */
#pragma once

#include <variant>

// The following code should work in C++17, but it currently failing with
// Apple LLVM 10.0.

// template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
// template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;
//
// template <class... Fs>
// auto make_visitor(Fs... fs)
// {
//     return overloaded<Fs...>(fs...);
// }

// so we do it this way instead...
template <class... Fs>
struct overload;

template <class F0, class... Frest>
struct overload<F0, Frest...> : F0, overload<Frest...>
{
    overload(F0 f0, Frest... rest) : F0(f0), overload<Frest...>(rest...) {}

    using F0::operator();
    using overload<Frest...>::operator();
};

template <class F0>
struct overload<F0> : F0
{
    overload(F0 f0) : F0(f0) {}

    using F0::operator();
};

template <class... Fs>
auto make_visitor(Fs... fs)
{
    return overload<Fs...>(fs...);
}
