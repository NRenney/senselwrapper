/**
 * Author: Nathan Renney
 * Desc: Light wrapper around sensel.h to work with sensel morph as an interface
 *
 * LICENSE: ToDo
 */
#pragma once

#include "sensel.h"
#include <iostream>


namespace sensel {

struct Ords{
  float x;
  float y;

  Ords(float x_, float y_) : x(x_), y(y_){}; 
};

typedef SenselFrameData Frame;

class MorphInterface {
private:
  SenselDeviceList m_list;
  SenselSensorInfo m_sensorInfo;
	SENSEL_HANDLE m_handle = NULL;
	SenselFrameData *m_pframe = nullptr;

  bool m_exit = true;
	//Get a list of avaialble Sensel devices
public:
  /**
   * contruct new MorphInterface by device ID
  */
  // Need to configure the frame content
  MorphInterface(int deviceID, bool startScanningImmediately);
  /**
   * begin scanning device
  */
  void startScanning();
  /**
   read from device
  */
  void setLED(unsigned char ledID, unsigned short brightness);

  void handleRead(std::function<void (Frame)> readCallback);

  void setExitFlag() { m_exit = false; };
};

}// sensel